﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace TaskDataLibTests.Common
{
    public class CustomTraceListener : TraceListener
    {
        public override void Write(string message)
        {
            Console.Write(">>> " + message);
        }

        public override void WriteLine(string message)
        {
            Console.WriteLine(">>> " + message);
        }
    }
}
