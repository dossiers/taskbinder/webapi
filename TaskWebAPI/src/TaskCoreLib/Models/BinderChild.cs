﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskCoreLib.Models
{
    public class BinderChild
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        // Data copy
        public void CopyDataFrom(BinderChild source)
        {
            BinderId = source.BinderId;
            BoardId = source.BoardId;
            BoardTitle = source.BoardTitle;
            Created = source.Created;
            Updated = source.Updated;
            Deleted = source.Deleted;
        }

        // Parent's type should be TaskBinder
        // public ulong ParentId { get; set; }
        public ulong BinderId { get; set; }
        // Child should be TaskBoard.
        // public ulong ChildId { get; set; }
        public ulong BoardId { get; set; }

        // For convenience...
        public string BoardTitle { get; set; }

        // public BoardSize ChildSize { get; set; }  // Child's size is always the same as the parent.  --> TBD: How to enforce it???
        // public int ChildZOrder { get; set; }
        public int BoardZOrder { get; set; }
        // tbd:
        // overrideable access level, etc. ....
        // ... 

        public long Created { get; set; }
        public long Updated { get; set; }
        public long Deleted { get; set; }
    }
}
