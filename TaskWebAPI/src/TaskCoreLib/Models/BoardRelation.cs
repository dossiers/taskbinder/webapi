﻿using HoloLC.TaskCoreLib.Common;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskCoreLib.Models
{
    // TBD:
    // Just use BoardChild table ???

    // Task dependency.
    public class BoardRelation
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        // Data copy
        public void CopyDataFrom(BoardRelation source)
        {
            DependentBoardId = source.DependentBoardId;
            LeadingBoardId = source.LeadingBoardId;
            LeadingBoardTitle = source.LeadingBoardTitle;
            IsBlocked = source.IsBlocked;
            Created = source.Created;
            Updated = source.Updated;
            Deleted = source.Deleted;
        }

        // Dependent task
        public ulong DependentBoardId { get; set; }
        // Depending task.
        public ulong LeadingBoardId { get; set; }

        // For convenience...
        public string LeadingBoardTitle { get; set; }

        // true, if the dependent task is blocked by leading task. 
        public bool IsBlocked { get; set; }

        // Display the relationship in a board???
        // public ElementPosition LeadingBoardPosition { get; set; }
        // public int LeadingBoardZOrder { get; set; }
        // ...

        public long Created { get; set; }
        public long Updated { get; set; }
        public long Deleted { get; set; }
    }
}
