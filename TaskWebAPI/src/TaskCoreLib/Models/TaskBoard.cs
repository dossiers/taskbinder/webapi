﻿using HoloLC.TaskCoreLib.Common;
using HoloLC.TaskCoreLib.Core;
using HoloLC.TaskCoreLib.Models.Core;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskCoreLib.Models
{
    // TaskBaord represents a "to do task".
    public class TaskBoard : TaskBase, ITask
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        // Data copy
        public void CopyDataFrom(TaskBoard source)
        {
            base.CopyDataFrom(source);
            Label = source.Label;
            CreatedBy = source.CreatedBy;
            AssignedTo = source.AssignedTo;
            CloneParent = source.CloneParent;
            Type = source.Type;
            CurrentView = source.CurrentView;
            Priority = source.Priority;
            Urgency = source.Urgency;
            Location = source.Location;
            StartDate = source.StartDate;
            EndDate = source.EndDate;
            DueDate = source.DueDate;
            IsAlertEnabled = source.IsAlertEnabled;
            State = source.State;
            ResolvedTime = source.ResolvedTime;
            ClosedTime = source.ClosedTime;
            IsTerminal = source.IsTerminal;
        }

        // TBD:
        // Move these proerties into a separate object?? Out of "Board" ???

        // Primary "tag" (e.g., "work", "personal", etc.)
        //  --> one-many tagging????  --> TBD: Create separate "tag" and "board-tag" tables...
        // (what aobut binder????)
        public string Label { get; set; }
        // public IList<string> Labels { get; set; }
        // ....

        public ulong CreatedBy { get; set; }   // vs. Owner ????
        public ulong AssignedTo { get; set; }


        // tBD:
        // "do it again"???
        // If CloneParent != null, this task is a "do it again" for other task.
        public ulong CloneParent { get; set; }
        // ...

        // ???
        public TaskType Type { get; set; }

        // tbd:
        // board type? "view"? - user-created mosaic/collage, 
        //                       priority list, urgency list, status-based (to do, doing, done), 
        //                       qudarants (priority+urgency axes), etc. ...
        public BoardView CurrentView { get; set; }
        // ...
        public bool IsDynamic
        {
            get
            {
                return CurrentView.IsDynamic();
            }
        }
        // ...


        // ???
        public int Priority { get; set; }
        public int Urgency { get; set; }
        // ???
        // TBD: distinguish between recurring task and task instance ???
        // ....
        // TBD: Use cron expression or something like that ????
        // public bool IsRecurring { get; set; }
        // public uint RecurriengInterval { get; set; }
        // ???

        // ???
        public GeoDisk Location { get; set; }

        public long StartDate { get; set; }   // ???
        public long EndDate { get; set; }
        public long DueDate { get; set; }   // EndDate <= DueDate ???
        public bool IsAlertEnabled { get; set; }

        // Used when it is included in another (parent) board???
        // Just use Title???
        // public string Synopsis { get; set; }

        // "Phase"/stage, etc ????
        // --> Use binder, maybe???

        // public bool IsCompleted { get; set; }
        public TaskState State { get; set; }
        // ???
        // public long CompletedTime { get; set; }
        public long ResolvedTime { get; set; }
        public long ClosedTime { get; set; }
        // ...

        // "Attachment" ???
        // ....

        public bool IsTerminal { get; set; } = false;   // If true, it cannot have child boards.

        // tbd:
        // how to specify parent board(1) - child board(n) relationship ???
        // child board location, z-order,  overrideable access level, ....
        // ... 

        // tbd:
        // task dependencies ????
        // ....
    }
}
