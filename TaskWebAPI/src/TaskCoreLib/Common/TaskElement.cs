﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskCoreLib.Common
{
    public abstract class TaskElement
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        // Data copy
        public void CopyDataFrom(TaskElement source)
        {
            Id = source.Id;
            Title = source.Title;
            Description = source.Description;
            Created = source.Created;
            Updated = source.Updated;
            Deleted = source.Deleted;
        }

        public ulong Id { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }

        public long Created { get; set; }
        public long Updated { get; set; }
        public long Deleted { get; set; }
    }
}
