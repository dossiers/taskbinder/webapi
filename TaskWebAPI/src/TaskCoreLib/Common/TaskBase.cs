﻿using HoloLC.TaskCoreLib.Core;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskCoreLib.Common
{
    public abstract class TaskBase : TaskElement
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        // Data copy
        public void CopyDataFrom(TaskBase source)
        {
            base.CopyDataFrom(source);
            Owner = source.Owner;
            AccessLevel = source.AccessLevel;
            Size = source.Size;
        }

        public ulong Owner { get; set; }
        public AccessLevel AccessLevel { get; set; }

        // EF does not support struct property, by default.
        // Instead of using attributes, we will just use a workaround.
        // public BoardSize Size { get; set; }
        private BoardSize size = new BoardSize();
        public BoardSize Size
        {
            get
            {
                return size;
            }
            set
            {
                size = value;
            }
        }
        public uint Width
        {
            get
            {
                return size.Width;
            }
            set
            {
                size.Width = value;
            }
        }
        public uint Height
        {
            get
            {
                return size.Height;
            }
            set
            {
                size.Height = value;
            }
        }

        // ... 

    }
}
