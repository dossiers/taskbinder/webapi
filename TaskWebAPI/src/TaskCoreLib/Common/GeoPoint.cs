﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskCoreLib.Common
{
    // [ComplexType]
    public struct GeoPoint
    {
        public static readonly GeoPoint Origin = new GeoPoint(0f, 0f);

        public GeoPoint(float x, float y)
        {
            X = x;
            Y = y;
        }

        // X=Lat, Y=Lon
        public float X { get; set; }
        public float Y { get; set; }

        public override string ToString()
        {
            return $"({X},{Y})";
        }
    }
}
