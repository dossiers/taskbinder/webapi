﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskCoreLib.Core
{
    // ???
    public enum TaskItemType
    {
        Unknown = 0,
        QuickTask = 1,       // e.g., "Call Jennifer". Has start time but no duration. 
        RegularTask = 2,     // 
        Appointment = 3,
        Event = 4,
        Alert = 5,           // Alarm clock, ...
        // ...

        // Note, purpose, etc. ???
        // ....

    }

    public static class TaskItemTypes
    {
        public static string ToName(this TaskItemType taskItemType)
        {
            try {
                return Enum.GetName(typeof(TaskItemType), taskItemType);
            } catch (Exception) {
                return null;
            }
        }
        public static TaskItemType ToTaskItemType(this string value)
        {
            try {
                return (TaskItemType)Enum.Parse(typeof(TaskItemType), value);
            } catch (Exception) {
                return TaskItemType.Unknown;
            }
        }
    }

}

