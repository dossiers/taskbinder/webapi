﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskCoreLib.Core
{
    // ???
    public enum TaskType
    {
        Unknown = 0,
        Generic = 1,   // general...
        Task = 2,
        Appointment = 3,
        Event = 4,
        // ...

    }

    public static class TaskTypes
    {
        public static string ToName(this TaskType taskType)
        {
            try {
                return Enum.GetName(typeof(TaskType), taskType);
            } catch (Exception) {
                return null;
            }
        }
        public static TaskType ToTaskType(this string value)
        {
            try {
                return (TaskType)Enum.Parse(typeof(TaskType), value);
            } catch (Exception) {
                return TaskType.Unknown;
            }
        }
    }

}
