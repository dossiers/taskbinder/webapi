﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using HoloLC.TaskDataLib.DB;

namespace taskwebapi.Migrations
{
    [DbContext(typeof(TaskDbContext))]
    [Migration("20160908194305_InitialMigration")]
    partial class InitialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431");

            modelBuilder.Entity("HoloLC.TaskCoreLib.Common.ElementPosition", b =>
                {
                    b.Property<int>("X");

                    b.Property<int>("Y");

                    b.HasKey("X", "Y");

                    b.ToTable("ElementPosition");
                });

            modelBuilder.Entity("HoloLC.TaskCoreLib.Models.BinderChild", b =>
                {
                    b.Property<ulong>("BinderId");

                    b.Property<ulong>("BoardId");

                    b.Property<string>("BoardTitle");

                    b.Property<int>("BoardZOrder");

                    b.Property<long>("Created");

                    b.Property<long>("Deleted");

                    b.Property<long>("Updated");

                    b.HasKey("BinderId", "BoardId");

                    b.ToTable("BinderChildren");
                });

            modelBuilder.Entity("HoloLC.TaskCoreLib.Models.BoardChild", b =>
                {
                    b.Property<ulong>("ParentId");

                    b.Property<ulong>("ChildId");

                    b.Property<int?>("ChildPositionX");

                    b.Property<int?>("ChildPositionY");

                    b.Property<string>("ChildTitle");

                    b.Property<int>("ChildZOrder");

                    b.Property<long>("Created");

                    b.Property<long>("Deleted");

                    b.Property<long>("Updated");

                    b.HasKey("ParentId", "ChildId");

                    b.HasIndex("ChildPositionX", "ChildPositionY");

                    b.ToTable("BoardChildren");
                });

            modelBuilder.Entity("HoloLC.TaskCoreLib.Models.BoardRelation", b =>
                {
                    b.Property<ulong>("LeadingBoardId");

                    b.Property<ulong>("DependentBoardId");

                    b.Property<long>("Created");

                    b.Property<long>("Deleted");

                    b.Property<bool>("IsBlocked");

                    b.Property<string>("LeadingBoardTitle");

                    b.Property<long>("Updated");

                    b.HasKey("LeadingBoardId", "DependentBoardId");

                    b.ToTable("BoardRelations");
                });

            modelBuilder.Entity("HoloLC.TaskCoreLib.Models.TaskBinder", b =>
                {
                    b.Property<ulong>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessLevel");

                    b.Property<long>("Created");

                    b.Property<long>("Deleted");

                    b.Property<string>("Description");

                    b.Property<uint>("Height");

                    b.Property<ulong>("HomeBoard");

                    b.Property<ulong>("Owner");

                    b.Property<string>("Title");

                    b.Property<long>("Updated");

                    b.Property<uint>("Width");

                    b.HasKey("Id");

                    b.ToTable("TaskBinders");
                });

            modelBuilder.Entity("HoloLC.TaskCoreLib.Models.TaskBoard", b =>
                {
                    b.Property<ulong>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessLevel");

                    b.Property<ulong>("AssignedTo");

                    b.Property<long>("ClosedTime");

                    b.Property<long>("Created");

                    b.Property<ulong>("CreatedBy");

                    b.Property<long>("Deleted");

                    b.Property<string>("Description");

                    b.Property<long>("DueDate");

                    b.Property<long>("EndDate");

                    b.Property<uint>("Height");

                    b.Property<bool>("IsAlertEnabled");

                    b.Property<bool>("IsTerminal");

                    b.Property<string>("Label");

                    b.Property<ulong>("Owner");

                    b.Property<int>("Priority");

                    b.Property<long>("ResolvedTime");

                    b.Property<long>("StartDate");

                    b.Property<int>("Status");

                    b.Property<string>("Title");

                    b.Property<int>("Type");

                    b.Property<long>("Updated");

                    b.Property<int>("Urgency");

                    b.Property<uint>("Width");

                    b.HasKey("Id");

                    b.ToTable("TaskBoards");
                });

            modelBuilder.Entity("HoloLC.TaskCoreLib.Models.TaskItem", b =>
                {
                    b.Property<ulong>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<ulong>("Board");

                    b.Property<long>("Created");

                    b.Property<long>("Deleted");

                    b.Property<int?>("PositionX");

                    b.Property<int?>("PositionY");

                    b.Property<long>("Updated");

                    b.Property<int>("ZOrder");

                    b.HasKey("Id");

                    b.HasIndex("PositionX", "PositionY");

                    b.ToTable("TaskItems");
                });

            modelBuilder.Entity("HoloLC.TaskCoreLib.Models.BoardChild", b =>
                {
                    b.HasOne("HoloLC.TaskCoreLib.Common.ElementPosition", "ChildPosition")
                        .WithMany()
                        .HasForeignKey("ChildPositionX", "ChildPositionY");
                });

            modelBuilder.Entity("HoloLC.TaskCoreLib.Models.TaskItem", b =>
                {
                    b.HasOne("HoloLC.TaskCoreLib.Common.ElementPosition", "Position")
                        .WithMany()
                        .HasForeignKey("PositionX", "PositionY");
                });
        }
    }
}
