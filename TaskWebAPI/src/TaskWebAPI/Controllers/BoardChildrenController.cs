﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HoloLC.TaskDataLib.Managers;
using HoloLC.TaskDataLib.Repositories;
using HoloLC.TaskCoreLib.Models;
using NLog;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HoloLC.TaskWebAPI.Controllers
{
    // [Route("api/board/{parent}/child/{child}")]
    [Route("api/[controller]")]
    public class BoardChildrenController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public BoardChildrenController(IDataRepositoryManager repositoryManager)
        {
            BoardChildRepository = repositoryManager.BoardChildRepository;
        }
        public IBoardChildRepository BoardChildRepository { get; set; }


        [HttpGet("parent/{parent}")]
        public async Task<IEnumerable<BoardChild>> FindChildrenAsync(ulong parent)
        {
            var boardChildren = await BoardChildRepository.FindChildrenAsync(parent);
            return boardChildren;
        }

        [HttpPost]
        public async Task PostAsync([FromBody] BoardChild boardChild)
        {
            var suc = await BoardChildRepository.AddBoardChildAsync(boardChild);

            // tbd:
            if (suc) {

            } else {

            }
        }

        //[HttpPut("{id}")]
        //public void Put(ulong id, [FromBody] BoardChild boardChild)
        //{
        //    var suc = BoardChildRepository.ReplaceBoardChild(boardChild, true);

        //    // tbd:
        //    if (suc) {

        //    } else {

        //    }
        //}

        [HttpPut("board/{parentId}/child/{childId}")]
        public async Task PutAsync(ulong parentId, ulong childId, [FromBody] BoardChild boardChild)
        {
            // assert parent == boardChild.ParentId && child = boardChild.ChildId
            var suc = await BoardChildRepository.ReplaceBoardChildAsync(boardChild, true);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpDelete]
        public async Task<int> RemoveAllAsync(ulong parent)
        {
            return await BoardChildRepository.RemoveByParentAsync(parent);
        }

        //[HttpDelete("{id}")]
        //public void Delete(ulong parentId, ulong childId)
        //{
        //    BoardChildRepository.DeleteBoardChild(id);
        //}
        [HttpDelete("board/{parentId}/child/{childId}")]
        public async Task<IActionResult> DeleteAsync(ulong parentId, ulong childId)
        {
            var deleted = await BoardChildRepository.DeleteBoardChildAsync(parentId, childId);
            if (deleted == null) {
                return NotFound();
            } else {
                return new ObjectResult(deleted);   // ???
            }
        }
    }
}
