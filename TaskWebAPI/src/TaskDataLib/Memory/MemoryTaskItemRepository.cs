﻿using HoloLC.TaskDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.TaskCoreLib.Common;
using HoloLC.TaskCoreLib.Core;
using HoloLC.TaskCoreLib.Models;
using System.Collections.Concurrent;
using NLog;

namespace HoloLC.TaskDataLib.Memory
{
    public class MemoryTaskItemRepository : ITaskItemRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly ConcurrentDictionary<ulong, IDictionary<ulong, TaskItem>> itemMap = new ConcurrentDictionary<ulong, IDictionary<ulong, TaskItem>>();


        public async Task<TaskItem> GetItemAsync(ulong id, ulong board = 0)
        {
            if(board > 0UL) {
                IDictionary<ulong, TaskItem> map;
                if (itemMap.TryGetValue(board, out map)) {
                    if (map.ContainsKey(id)) {
                        var taskItem = map[board];
                        return taskItem;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                throw new NotImplementedException();
            }
        }

        public async Task<IEnumerable<TaskItem>> FindAllAsync(ulong board)
        {
            IDictionary<ulong, TaskItem> map;
            if (itemMap.TryGetValue(board, out map)) {
                var collection = map.Values;
                return collection;
            } else {
                return null;
            }
        }

        //public IEnumerable<TaskItem> FindByType(ulong board, ItemType type)
        //{
        //    throw new NotImplementedException();
        //}


        public async Task<bool> AddItemAsync(TaskItem item)
        {
            var id = item.Id;
            var board = item.Board;
            IDictionary<ulong, TaskItem> map;
            if (itemMap.TryGetValue(board, out map)) {
                // tbd:
                // This does not gurantee the uniqueness of the id across different boards....
                if (map.ContainsKey(id)) {
                    System.Diagnostics.Debug.WriteLine($"TaskItem cannot be added because an item with the given id already exists: id = {id}.");
                    return false;
                }
            } else {
                map = new Dictionary<ulong, TaskItem>();
                itemMap[board] = map;
            }

            // TBD: Update the Created/Updated field?
            map[id] = item;
            return true;
        }

        public async Task<bool> ReplaceItemAsync(TaskItem taskItem, bool createIfNotExist = false)
        {
            var id = taskItem.Id;
            var board = taskItem.Board;
            IDictionary<ulong, TaskItem> map;
            if (itemMap.TryGetValue(board, out map)) {
            } else {
                if (createIfNotExist) {
                    map = new Dictionary<ulong, TaskItem>();
                    itemMap[board] = map;
                } else {
                    return false;
                }
            }

            // TBD: Update the Updated field?
            map[id] = taskItem;
            return true;
        }


        public async Task<TaskItem> DeleteItemAsync(ulong id, ulong board = 0)
        {
            if (board > 0UL) {
                IDictionary<ulong, TaskItem> map;
                if (itemMap.TryGetValue(board, out map)) {
                    var taskItem = map[id];
                    if (map.Remove(id)) {
                        return taskItem;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                throw new NotImplementedException();
            }
        }

        public async Task<int> RemoveAllAsync(ulong board)
        {
            IDictionary<ulong, TaskItem> map;
            if (itemMap.TryRemove(board, out map)) {
                return map.Count;
            } else {
                return 0;
            }
        }

        //public int RemoveByType(ulong board, ItemType type)
        //{
        //    throw new NotImplementedException();
        //}

    }
}
