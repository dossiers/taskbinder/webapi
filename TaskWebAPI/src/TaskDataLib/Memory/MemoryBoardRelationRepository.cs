﻿using HoloLC.TaskDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.TaskCoreLib.Common;
using HoloLC.TaskCoreLib.Models;
using System.Collections.Concurrent;
using NLog;

namespace HoloLC.TaskDataLib.Memory
{
    public class MemoryBoardRelationRepository : IBoardRelationRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly ConcurrentDictionary<ulong, IDictionary<ulong, BoardRelation>> boardRelationMap = new ConcurrentDictionary<ulong, IDictionary<ulong, BoardRelation>>();

        public async Task<IEnumerable<BoardRelation>> FindLeadingBoardsAsync(ulong dependentBoardId)
        {
            IDictionary<ulong, BoardRelation> map;
            if (boardRelationMap.TryGetValue(dependentBoardId, out map)) {
                var collection = map.Values;
                return collection;
            } else {
                return null;
            }
        }

        public async Task<bool> AddBoardRelationAsync(BoardRelation boardRelation)
        {
            var dependentBoardId = boardRelation.DependentBoardId;
            var leadingBoardId = boardRelation.LeadingBoardId;
            IDictionary<ulong, BoardRelation> map;
            if (boardRelationMap.TryGetValue(dependentBoardId, out map)) {
                // tbd:
                // This does not gurantee the uniqueness of the leadingBoardId across different dependentBoardIds....
                if (map.ContainsKey(leadingBoardId)) {
                    System.Diagnostics.Debug.WriteLine($"BoardRelation cannot be added because an item with the given leadingBoardId already exists: leadingBoardId = {leadingBoardId}.");
                    return false;
                }
            } else {
                map = new Dictionary<ulong, BoardRelation>();
                boardRelationMap[dependentBoardId] = map;
            }

            // TBD: Update the Created/Updated field?
            map[leadingBoardId] = boardRelation;
            return true;
        }

        public async Task<bool> ReplaceBoardRelationAsync(BoardRelation boardRelation, bool createIfNotExist = false)
        {
            var dependentBoardId = boardRelation.DependentBoardId;
            var leadingBoardId = boardRelation.LeadingBoardId;
            IDictionary<ulong, BoardRelation> map;
            if (boardRelationMap.TryGetValue(dependentBoardId, out map)) {
            } else {
                if (createIfNotExist) {
                    map = new Dictionary<ulong, BoardRelation>();
                    boardRelationMap[dependentBoardId] = map;
                } else {
                    return false;
                }
            }

            // TBD: Update the Updated field?
            map[leadingBoardId] = boardRelation;
            return true;
        }

        public async Task<BoardRelation> DeleteBoardRelationAsync(ulong dependentBoardId, ulong leadingBoardId)
        {
            IDictionary<ulong, BoardRelation> map;
            if (boardRelationMap.TryGetValue(dependentBoardId, out map)) {
                var boardRelation = map[leadingBoardId];
                if (map.Remove(leadingBoardId)) {
                    return boardRelation;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }

        public async Task<int> RemoveByDependentBoardAsync(ulong dependentBoardId)
        {
            IDictionary<ulong, BoardRelation> map;
            if (boardRelationMap.TryRemove(dependentBoardId, out map)) {
                return map.Count;
            } else {
                return 0;
            }
        }

    }
}
