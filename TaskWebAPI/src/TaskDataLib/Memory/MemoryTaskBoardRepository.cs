﻿using HoloLC.TaskDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.TaskCoreLib.Common;
using HoloLC.TaskCoreLib.Core;
using HoloLC.TaskCoreLib.Models;
using System.Collections.Concurrent;
using NLog;

namespace HoloLC.TaskDataLib.Memory
{
    public class MemoryTaskBoardRepository : ITaskBoardRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly ConcurrentDictionary<ulong, IDictionary<ulong, TaskBoard>> taskBoardMap = new ConcurrentDictionary<ulong, IDictionary<ulong, TaskBoard>>();


        public async Task<TaskBoard> GetItemAsync(ulong id, ulong owner = 0UL)
        {
            if (owner > 0UL) {
                IDictionary<ulong, TaskBoard> map;
                if (taskBoardMap.TryGetValue(owner, out map)) {
                    if (map.ContainsKey(id)) {
                        var taskBoard = map[owner];
                        return taskBoard;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                throw new NotImplementedException();
            }
        }

        public async Task<IEnumerable<TaskBoard>> FindAllByOwnerAsync(ulong owner)
        {
            IDictionary<ulong, TaskBoard> map;
            if (taskBoardMap.TryGetValue(owner, out map)) {
                var collection = map.Values;
                return collection;
            } else {
                return null;
            }
        }


        public async Task<bool> AddItemAsync(TaskBoard taskBoard)
        {
            var id = taskBoard.Id;
            var owner = taskBoard.Owner;
            IDictionary<ulong, TaskBoard> map;
            if (taskBoardMap.TryGetValue(owner, out map)) {
                // tbd:
                // This does not gurantee the uniqueness of the id across different owners....
                if (map.ContainsKey(id)) {
                    System.Diagnostics.Debug.WriteLine($"TaskBoard cannot be added because an item with the given id already exists: id = {id}.");
                    return false;
                }
            } else {
                map = new Dictionary<ulong, TaskBoard>();
                taskBoardMap[owner] = map;
            }

            // TBD: Update the Created/Updated field?
            map[id] = taskBoard;
            return true;
        }

        public async Task<bool> ReplaceItemAsync(TaskBoard taskBoard, bool createIfNotExist = false)
        {
            var id = taskBoard.Id;
            var owner = taskBoard.Owner;
            IDictionary<ulong, TaskBoard> map;
            if (taskBoardMap.TryGetValue(owner, out map)) {
            } else {
                if (createIfNotExist) {
                    map = new Dictionary<ulong, TaskBoard>();
                    taskBoardMap[owner] = map;
                } else {
                    return false;
                }
            }

            // TBD: Update the Updated field?
            map[id] = taskBoard;
            return true;
        }


        public async Task<TaskBoard> DeleteItemAsync(ulong id, ulong owner = 0UL)
        {
            if (owner > 0UL) {
                IDictionary<ulong, TaskBoard> map;
                if (taskBoardMap.TryGetValue(owner, out map)) {
                    var taskBoard = map[id];
                    if (map.Remove(id)) {
                        return taskBoard;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                throw new NotImplementedException();
            }
        }

        public async Task<int> RemoveAllByOwnerAsync(ulong owner)
        {
            IDictionary<ulong, TaskBoard> map;
            if (taskBoardMap.TryRemove(owner, out map)) {
                return map.Count;
            } else {
                return 0;
            }
        }

        //public int RemoveByType(ulong owner, ItemType type)
        //{
        //    throw new NotImplementedException();
        //}

    }
}
