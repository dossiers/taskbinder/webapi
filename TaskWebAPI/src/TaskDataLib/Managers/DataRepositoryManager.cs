﻿using HoloLC.TaskDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskDataLib.Managers
{
    public interface IDataRepositoryManager
    {
        ITaskBinderRepository TaskBinderRepository { get; }
        IBinderChildRepository BinderChildRepository { get; }
        ITaskBoardRepository TaskBoardRepository { get; }
        IBoardChildRepository BoardChildRepository { get; }
        ITaskItemRepository TaskItemRepository { get; }
        IBoardRelationRepository BoardRelationRepository { get; }

    }
}
