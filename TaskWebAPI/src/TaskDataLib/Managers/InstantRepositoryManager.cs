﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskDataLib.Managers
{
    // Normally, IDataRepositoryManager should be used as a type.
    public interface IInstantRepositoryManager : IDataRepositoryManager
    {
        // Dangerous operations.

        // Create all tables, if relavant.
        Task CreateAllAsync(bool recreateIfPresent = false);
        // Delete all tables, if relevannt.
        Task DeleteAllAsync();
        // Remove data from all tables, if relevant.
        Task PurgeAllAsync(bool createIfAbsent = false);
    }
}
