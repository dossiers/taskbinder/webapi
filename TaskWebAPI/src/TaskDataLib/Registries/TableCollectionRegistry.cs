﻿using Amazon.DynamoDBv2;
using Amazon.Runtime;
using AWSCore.DynamoBase.Clients;
using AWSCore.DynamoBase.Clients.Impl;
using AWSCore.DynamoBase.Registries;
using AWSCore.DynamoBase.Registries.Base;
using AWSCore.DynamoBase.Tables.Core;
using AWSCore.DynamoBase.Tables.Impl;
using HoloLC.TaskDataLib.Dynamo.Tables;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskDataLib.Registries
{
    public sealed class TableCollectionRegistry : BaseTableCollectionRegistry, IInstantCollectionRegistry
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static TableCollectionRegistry Instance { get; } = new TableCollectionRegistry();
        private TableCollectionRegistry()
        {
        }

        protected override ITableCollection BuildTableCollection(string serviceURL, string tableSuffix)
        {
            Logger.Warn($"BuildTableCollection() serviceURL = {serviceURL}; tableSuffix = {tableSuffix}.");

            AmazonDynamoDBConfig devDynamoConfig = new AmazonDynamoDBConfig();
            devDynamoConfig.ServiceURL = serviceURL;
            // etc..

            // TBD: What's the best way to specify AWS credentials in the runtime environment ?????
            // AWSCredentials credentials = new StoredProfileAWSCredentials();   // Default profile.
            AWSCredentials credentials = new StoredProfileAWSCredentials("dynamo", "aws-credentials");
            AmazonDynamoDBClient devDynamoClient = new AmazonDynamoDBClient(credentials, devDynamoConfig);

            // TBD: AmazonDynamoDBClient or IDynamoDBClientContext should really be injected rather than be constructed here....
            IDynamoDBClientContext clientContext = new DefaultDynamoDBClientContext(devDynamoClient);
            ITableCollection tableCollection = new EnumerableTableCollection();
            tableCollection.SetTable(DynamoBinderChildTable.TABLE_KEY, new DynamoBinderChildTable(clientContext, $"{DynamoBinderChildTable.TABLE_KEY}.{tableSuffix}"));
            tableCollection.SetTable(DynamoBoardChildTable.TABLE_KEY, new DynamoBoardChildTable(clientContext, $"{DynamoBoardChildTable.TABLE_KEY}.{tableSuffix}"));
            tableCollection.SetTable(DynamoBoardRelationTable.TABLE_KEY, new DynamoBoardRelationTable(clientContext, $"{DynamoBoardRelationTable.TABLE_KEY}.{tableSuffix}"));
            tableCollection.SetTable(DynamoTaskBinderTable.TABLE_KEY, new DynamoTaskBinderTable(clientContext, $"{DynamoTaskBinderTable.TABLE_KEY}.{tableSuffix}"));
            tableCollection.SetTable(DynamoTaskBoardTable.TABLE_KEY, new DynamoTaskBoardTable(clientContext, $"{DynamoTaskBoardTable.TABLE_KEY}.{tableSuffix}"));
            tableCollection.SetTable(DynamoTaskItemTable.TABLE_KEY, new DynamoTaskItemTable(clientContext, $"{DynamoTaskItemTable.TABLE_KEY}.{tableSuffix}"));

            return tableCollection;
        }

    }
}
