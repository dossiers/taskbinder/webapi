﻿using HoloLC.TaskDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.TaskCoreLib.Models;
using NLog;
using HoloLC.TaskDataLib.DB;

namespace HoloLC.TaskDataLib.Persistent
{
    public class PersistentTaskBoardRepository : ITaskBoardRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly TaskDbContext dbContext;

        public PersistentTaskBoardRepository(TaskDbContext dbContext)
        {
            this.dbContext = dbContext;
        }


        public async Task<IEnumerable<TaskBoard>> FindAllByOwnerAsync(ulong owner)
        {
            Logger.Trace($"FindAll() owner = {owner}.");

            var queryable = dbContext.TaskBoards
                .Select(o => o)
                .Where(o => o.Owner == owner);
            if (queryable != null) {
                return queryable.ToList();
            }
            Logger.Info($"FindAll() returing null. owner = {owner}.");
            return null;
        }

        public async Task<TaskBoard> GetItemAsync(ulong id, ulong owner = 0)
        {
            Logger.Trace($"GetItem() id = {id}; owner = {owner}.");

            var taskBoard = dbContext.TaskBoards.FirstOrDefault(o => o.Id == id && o.Owner == owner);
            return taskBoard;
        }

        public async Task<bool> AddItemAsync(TaskBoard taskBoard)
        {
            Logger.Trace($"AddItem() ptaskBoard = {taskBoard}.");

            dbContext.TaskBoards.Add(taskBoard);
            var saved = dbContext.SaveChanges();
            if (saved > 0) {
                System.Diagnostics.Debug.WriteLine($"MermoShard saved: id = {taskBoard.Id}. owner = {taskBoard.Owner}.");
                Logger.Debug($"MermoShard saved: id = {taskBoard.Id}. owner = {taskBoard.Owner}.");
                return true;
            } else {
                System.Diagnostics.Debug.WriteLine($"Failed to save MermoShard: id = {taskBoard.Id}. owner = {taskBoard.Owner}.");
                Logger.Warn($"Failed to save MermoShard: id = {taskBoard.Id}. owner = {taskBoard.Owner}.");
                return false;
            }
        }

        public async Task<bool> ReplaceItemAsync(TaskBoard taskBoard, bool createIfNotExist = false)
        {
            Logger.Trace($"ReplaceItem() ptaskBoard = {taskBoard}; createIfNotExist = {createIfNotExist}.");

            int saved = 0;
            var existingTaskBoard = dbContext.TaskBoards.FirstOrDefault(o => o.Owner == taskBoard.Owner && o.Id == taskBoard.Id);
            if (existingTaskBoard != null) {
                existingTaskBoard.CopyDataFrom(taskBoard);
                dbContext.TaskBoards.Update(existingTaskBoard);
                saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoShard replaced: id = {taskBoard.Id}. owner = {taskBoard.Owner}.");
                    Logger.Debug($"MermoShard replaced: id = {taskBoard.Id}. owner = {taskBoard.Owner}.");
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to replace MermoShard: id = {taskBoard.Id}. owner = {taskBoard.Owner}.");
                    Logger.Warn($"Failed to replace MermoShard: id = {taskBoard.Id}. owner = {taskBoard.Owner}.");
                }
            } else {
                if (createIfNotExist) {
                    dbContext.TaskBoards.Add(taskBoard);
                    saved = dbContext.SaveChanges();
                    if (saved > 0) {
                        System.Diagnostics.Debug.WriteLine($"New pickupbox added: id = {taskBoard.Id}. owner = {taskBoard.Owner}.");
                        Logger.Debug($"New pickupbox added: id = {taskBoard.Id}. owner = {taskBoard.Owner}.");
                    } else {
                        System.Diagnostics.Debug.WriteLine($"Failed to add new MermoShard: id = {taskBoard.Id}. owner = {taskBoard.Owner}.");
                        Logger.Warn($"Failed to add new MermoShard: id = {taskBoard.Id}. owner = {taskBoard.Owner}.");
                    }
                } else {
                    System.Diagnostics.Debug.WriteLine($"MermoShard not found: id = {taskBoard.Id}. owner = {taskBoard.Owner}.");
                    Logger.Warn($"MermoShard not found: id = {taskBoard.Id}. owner = {taskBoard.Owner}.");
                }
            }
            return (saved > 0);
        }


        public async Task<int> RemoveAllByOwnerAsync(ulong owner)
        {
            Logger.Trace($"RemoveAll() owner = {owner}.");

            var queryable = dbContext.TaskBoards
                .Select(o => o)
                .Where(o => o.Owner == owner);
            if (queryable != null) {
                var list = queryable.ToList();
                var count = list.Count;
                dbContext.TaskBoards.RemoveRange(list);
                var saved = dbContext.SaveChanges();
                System.Diagnostics.Debug.WriteLine($"MermoShard removed: target count = {count}; removed = {saved}. owner = {owner}.");
                Logger.Debug($"MermoShard removed: target count = {count}; removed = {saved}. owner = {owner}.");
                return saved;
            }
            return 0;
        }

        public async Task<TaskBoard> DeleteItemAsync(ulong id, ulong owner = 0)
        {
            Logger.Trace($"DeleteById() owner = {owner}; id = {id}.");

            var taskBoard = dbContext.TaskBoards.FirstOrDefault(o => o.Owner == owner && o.Id == id);
            if (taskBoard != null) {
                dbContext.TaskBoards.Remove(taskBoard);
                var saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoShard deleted: id = {taskBoard.Id}. owner = {taskBoard.Owner}.");
                    Logger.Debug($"MermoShard deleted: id = {taskBoard.Id}. owner = {taskBoard.Owner}.");
                    return taskBoard;
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to deleted pickupbox: id = {taskBoard.Id}. owner = {taskBoard.Owner}.");
                    Logger.Warn($"Failed to deleted pickupbox: id = {taskBoard.Id}. owner = {taskBoard.Owner}.");
                    return null;
                }
            }
            return null;
        }
    }
}
