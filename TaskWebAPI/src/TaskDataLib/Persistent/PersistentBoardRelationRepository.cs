﻿using HoloLC.TaskDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.TaskCoreLib.Models;
using NLog;
using HoloLC.TaskDataLib.DB;

namespace HoloLC.TaskDataLib.Persistent
{
    public class PersistentBoardRelationRepository : IBoardRelationRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly TaskDbContext dbContext;

        public PersistentBoardRelationRepository(TaskDbContext dbContext)
        {
            this.dbContext = dbContext;
        }


        public async Task<IEnumerable<BoardRelation>> FindLeadingBoardsAsync(ulong dependentBoardId)
        {
            Logger.Trace($"FindLeadingBoards() dependentBoardId = {dependentBoardId}.");

            var queryable = dbContext.BoardRelations
                .Select(o => o)
                .Where(o => o.DependentBoardId == dependentBoardId);
            if (queryable != null) {
                return queryable.ToList();
            }
            Logger.Info($"FindAll() returing null. dependentBoardId = {dependentBoardId}.");
            return null;
        }

        public async Task<bool> AddBoardRelationAsync(BoardRelation boardRelation)
        {
            Logger.Trace($"AddBoardRelation() boardRelation = {boardRelation}.");

            dbContext.BoardRelations.Add(boardRelation);
            var saved = dbContext.SaveChanges();
            if (saved > 0) {
                System.Diagnostics.Debug.WriteLine($"MermoShard saved: id = {boardRelation.LeadingBoardId}. dependentBoardId = {boardRelation.DependentBoardId}.");
                Logger.Debug($"MermoShard saved: id = {boardRelation.LeadingBoardId}. dependentBoardId = {boardRelation.DependentBoardId}.");
                return true;
            } else {
                System.Diagnostics.Debug.WriteLine($"Failed to save MermoShard: id = {boardRelation.LeadingBoardId}. dependentBoardId = {boardRelation.DependentBoardId}.");
                Logger.Warn($"Failed to save MermoShard: id = {boardRelation.LeadingBoardId}. dependentBoardId = {boardRelation.DependentBoardId}.");
                return false;
            }
        }

        public async Task<bool> ReplaceBoardRelationAsync(BoardRelation boardRelation, bool createIfNotExist = false)
        {
            Logger.Trace($"ReplaceItem() boardRelation = {boardRelation}; createIfNotExist = {createIfNotExist}.");

            int saved = 0;
            var existingBoardRelation = dbContext.BoardRelations.FirstOrDefault(o => o.DependentBoardId == boardRelation.DependentBoardId && o.LeadingBoardId == boardRelation.LeadingBoardId);
            if (existingBoardRelation != null) {
                existingBoardRelation.CopyDataFrom(boardRelation);
                dbContext.BoardRelations.Update(existingBoardRelation);
                saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoShard replaced: id = {boardRelation.LeadingBoardId}. dependentBoardId = {boardRelation.DependentBoardId}.");
                    Logger.Debug($"MermoShard replaced: id = {boardRelation.LeadingBoardId}. dependentBoardId = {boardRelation.DependentBoardId}.");
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to replace MermoShard: id = {boardRelation.LeadingBoardId}. dependentBoardId = {boardRelation.DependentBoardId}.");
                    Logger.Warn($"Failed to replace MermoShard: id = {boardRelation.LeadingBoardId}. dependentBoardId = {boardRelation.DependentBoardId}.");
                }
            } else {
                if (createIfNotExist) {
                    dbContext.BoardRelations.Add(boardRelation);
                    saved = dbContext.SaveChanges();
                    if (saved > 0) {
                        System.Diagnostics.Debug.WriteLine($"New pickupbox added: id = {boardRelation.LeadingBoardId}. dependentBoardId = {boardRelation.DependentBoardId}.");
                        Logger.Debug($"New pickupbox added: id = {boardRelation.LeadingBoardId}. dependentBoardId = {boardRelation.DependentBoardId}.");
                    } else {
                        System.Diagnostics.Debug.WriteLine($"Failed to add new MermoShard: id = {boardRelation.LeadingBoardId}. dependentBoardId = {boardRelation.DependentBoardId}.");
                        Logger.Warn($"Failed to add new MermoShard: id = {boardRelation.LeadingBoardId}. dependentBoardId = {boardRelation.DependentBoardId}.");
                    }
                } else {
                    System.Diagnostics.Debug.WriteLine($"MermoShard not found: id = {boardRelation.LeadingBoardId}. dependentBoardId = {boardRelation.DependentBoardId}.");
                    Logger.Warn($"MermoShard not found: id = {boardRelation.LeadingBoardId}. dependentBoardId = {boardRelation.DependentBoardId}.");
                }
            }
            return (saved > 0);
        }

        public async Task<int> RemoveByDependentBoardAsync(ulong dependentBoardId)
        {
            Logger.Trace($"RemoveByParent() dependentBoardId = {dependentBoardId}.");

            var queryable = dbContext.BoardRelations
                .Select(o => o)
                .Where(o => o.DependentBoardId == dependentBoardId);
            if (queryable != null) {
                var list = queryable.ToList();
                var count = list.Count;
                dbContext.BoardRelations.RemoveRange(list);
                var saved = dbContext.SaveChanges();
                System.Diagnostics.Debug.WriteLine($"MermoShard removed: target count = {count}; removed = {saved}. dependentBoardId = {dependentBoardId}.");
                Logger.Debug($"MermoShard removed: target count = {count}; removed = {saved}. dependentBoardId = {dependentBoardId}.");
                return saved;
            }
            return 0;
        }

        public async Task<BoardRelation> DeleteBoardRelationAsync(ulong dependentBoardId, ulong leadingBoardId)
        {
            Logger.Trace($"DeleteById() dependentBoardId = {dependentBoardId}; leadingBoardId = {leadingBoardId}.");

            var boardRelation = dbContext.BoardRelations.FirstOrDefault(o => o.DependentBoardId == dependentBoardId && o.LeadingBoardId == leadingBoardId);
            if (boardRelation != null) {
                dbContext.BoardRelations.Remove(boardRelation);
                var saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoShard deleted: leadingBoardId = {boardRelation.LeadingBoardId}. dependentBoardId = {boardRelation.DependentBoardId}.");
                    Logger.Debug($"MermoShard deleted: leadingBoardId = {boardRelation.LeadingBoardId}. dependentBoardId = {boardRelation.DependentBoardId}.");
                    return boardRelation;
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to deleted pickupbox: leadingBoardId = {boardRelation.LeadingBoardId}. dependentBoardId = {boardRelation.DependentBoardId}.");
                    Logger.Warn($"Failed to deleted pickupbox: leadingBoardId = {boardRelation.LeadingBoardId}. dependentBoardId = {boardRelation.DependentBoardId}.");
                    return null;
                }
            }
            return null;
        }

    }
}
