﻿using HoloLC.TaskCoreLib.Common;
using HoloLC.TaskCoreLib.Core;
using HoloLC.TaskCoreLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskDataLib.Repositories
{
    public interface ITaskBoardRepository
    {
        Task<TaskBoard> GetItemAsync(ulong id, ulong owner = 0UL);
        // IEnumerable<TaskBoard> FindAll();
        Task<IEnumerable<TaskBoard>> FindAllByOwnerAsync(ulong owner);
        // IEnumerable<TaskBoard> FindByAccessLevel(AccessLevel level);
        // IEnumerable<TaskBoard> FindByOwnerAndAccessLevel(ulong owner, AccessLevel level);

        // How/where to store actual content ???
        Task<bool> AddItemAsync(TaskBoard board);
        // TaskBoard CreateItem(ulong owner, AccessLevel level, BoardSize size, string title = null, string description = null, bool isTerminal = false);

        // "Overwrite" semantics If the item does not exist, create one. ?? Or, ignore and return null ??
        Task<bool> ReplaceItemAsync(TaskBoard taskBoard, bool createIfNotExist = false);
        // TaskBoard UpdateAccessLevel(ulong id, AccessLevel level);
        // TaskBoard UpdateTitle(ulong id, string title);

        Task<TaskBoard> DeleteItemAsync(ulong id, ulong owner = 0UL);
        Task<int> RemoveAllByOwnerAsync(ulong owner);
        // int RemoveByOwnerAndAccessLevel(ulong owner, AccessLevel level);

    }
}
