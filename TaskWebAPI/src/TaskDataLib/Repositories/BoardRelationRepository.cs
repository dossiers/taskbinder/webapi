﻿using HoloLC.TaskCoreLib.Common;
using HoloLC.TaskCoreLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace HoloLC.TaskDataLib.Repositories
{
    public interface IBoardRelationRepository
    {
        Task<IEnumerable<BoardRelation>> FindLeadingBoardsAsync(ulong parent);
        // IEnumerable<BoardRelation> FindDependentBoards(ulong boardRelation);

        Task<bool> AddBoardRelationAsync(BoardRelation boardRelation);
        // BoardRelation CreateBoardRelation(ulong dependentBoardId, ulong leadingBoardId, ElementPosition ChildPosition, int zOrder = 0);

        Task<bool> ReplaceBoardRelationAsync(BoardRelation boardRelation, bool createIfNotExist = false);

        Task<BoardRelation> DeleteBoardRelationAsync(ulong dependentBoardId, ulong leadingBoardId);
        Task<int> RemoveByDependentBoardAsync(ulong dependentBoardId);
        // int RemoveByLeadingBoard(ulong leadingBoardId);

    }
}
