﻿using Amazon.DynamoDBv2.DocumentModel;
using HoloLC.TaskCoreLib.Core;
using HoloLC.TaskCoreLib.Models;
using HoloLC.TaskCoreLib.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskDataLib.Dynamo.Tables.Converters
{
    public class TaskBoardConverter
    {
        public static Document ConvertToDocument(TaskBoard taskBoard)
        {
            if (taskBoard != null) {
                var document = new Document();
                document.Add(nameof(taskBoard.Id), taskBoard.Id);
                document.Add(nameof(taskBoard.Owner), taskBoard.Owner);
                document.Add(nameof(taskBoard.CreatedBy), taskBoard.CreatedBy);
                document.Add(nameof(taskBoard.AssignedTo), taskBoard.AssignedTo);
                if (taskBoard.Title != null) document.Add(nameof(taskBoard.Title), taskBoard.Title);
                if (taskBoard.Description != null) document.Add(nameof(taskBoard.Description), taskBoard.Description);
                if (taskBoard.AccessLevel.ToName() != null) document.Add(nameof(taskBoard.AccessLevel), taskBoard.AccessLevel.ToName());
                document.Add(nameof(taskBoard.CloneParent), taskBoard.CloneParent);
                if (taskBoard.Type.ToName() != null) document.Add(nameof(taskBoard.Type), taskBoard.Type.ToName());
                if (taskBoard.CurrentView.ToName() != null) document.Add(nameof(taskBoard.CurrentView), taskBoard.CurrentView.ToName());
                if (taskBoard.Label != null) document.Add(nameof(taskBoard.Label), taskBoard.Label);
                document.Add(nameof(taskBoard.IsAlertEnabled), taskBoard.IsAlertEnabled);
                document.Add(nameof(taskBoard.IsTerminal), taskBoard.IsTerminal);
                document.Add(nameof(taskBoard.Width), taskBoard.Width);
                document.Add(nameof(taskBoard.Height), taskBoard.Height);
                document.Add(nameof(taskBoard.Priority), taskBoard.Priority);
                document.Add(nameof(taskBoard.Urgency), taskBoard.Urgency);
                document.Add(nameof(taskBoard.Location), taskBoard.Location.ToSerializedString());
                if (taskBoard.State.ToName() != null) document.Add(nameof(taskBoard.State), taskBoard.State.ToName());
                document.Add(nameof(taskBoard.StartDate), taskBoard.StartDate);
                document.Add(nameof(taskBoard.EndDate), taskBoard.EndDate);
                document.Add(nameof(taskBoard.DueDate), taskBoard.DueDate);
                document.Add(nameof(taskBoard.ResolvedTime), taskBoard.ResolvedTime);
                document.Add(nameof(taskBoard.ClosedTime), taskBoard.ClosedTime);
                document.Add(nameof(taskBoard.Created), taskBoard.Created);
                document.Add(nameof(taskBoard.Updated), taskBoard.Updated);
                document.Add(nameof(taskBoard.Deleted), taskBoard.Deleted);
                return document;
            } else {
                return null;
            }
        }
        public static TaskBoard ConvertFromDocument(Document document)
        {
            if (document != null) {
                var taskBoard = new TaskBoard();
                taskBoard.Id = document[nameof(taskBoard.Id)].AsULong();
                taskBoard.Owner = document.ContainsKey(nameof(taskBoard.Owner)) ? document[nameof(taskBoard.Owner)].AsULong() : 0UL;
                taskBoard.CreatedBy = document.ContainsKey(nameof(taskBoard.CreatedBy)) ? document[nameof(taskBoard.CreatedBy)].AsULong() : 0UL;
                taskBoard.AssignedTo = document.ContainsKey(nameof(taskBoard.AssignedTo)) ? document[nameof(taskBoard.AssignedTo)].AsULong() : 0UL;
                taskBoard.Title = document.ContainsKey(nameof(taskBoard.Title)) ? document[nameof(taskBoard.Title)].AsString() : "";
                taskBoard.Description = document.ContainsKey(nameof(taskBoard.Description)) ? document[nameof(taskBoard.Description)].AsString() : "";
                taskBoard.AccessLevel = document.ContainsKey(nameof(taskBoard.AccessLevel)) ? document[nameof(taskBoard.AccessLevel)].AsString().ToAccessLevel() : AccessLevel.Unknown;
                taskBoard.CloneParent = document.ContainsKey(nameof(taskBoard.CloneParent)) ? document[nameof(taskBoard.CloneParent)].AsULong() : 0UL;
                taskBoard.Type = document.ContainsKey(nameof(taskBoard.Type)) ? document[nameof(taskBoard.Type)].AsString().ToTaskType() : TaskType.Unknown;
                taskBoard.CurrentView = document.ContainsKey(nameof(taskBoard.CurrentView)) ? document[nameof(taskBoard.CurrentView)].AsString().ToBoardView() : BoardView.Unknown;
                taskBoard.Label = document.ContainsKey(nameof(taskBoard.Label)) ? document[nameof(taskBoard.Label)].AsString() : "";
                taskBoard.IsAlertEnabled = document.ContainsKey(nameof(taskBoard.IsAlertEnabled)) ? document[nameof(taskBoard.IsAlertEnabled)].AsBoolean() : false;
                taskBoard.IsTerminal = document.ContainsKey(nameof(taskBoard.IsTerminal)) ? document[nameof(taskBoard.IsTerminal)].AsBoolean() : false;
                taskBoard.Width = document.ContainsKey(nameof(taskBoard.Width)) ? document[nameof(taskBoard.Width)].AsUInt() : 0U;
                taskBoard.Height = document.ContainsKey(nameof(taskBoard.Height)) ? document[nameof(taskBoard.Height)].AsUInt() : 0U;
                taskBoard.Priority = document.ContainsKey(nameof(taskBoard.Priority)) ? document[nameof(taskBoard.Priority)].AsInt() : 0;
                taskBoard.Urgency = document.ContainsKey(nameof(taskBoard.Urgency)) ? document[nameof(taskBoard.Urgency)].AsInt() : 0;
                taskBoard.Location = document.ContainsKey(nameof(taskBoard.Location)) ? document[nameof(taskBoard.Location)].AsString().ToGeoDisk() : GeoDisk.OriginPoint;
                taskBoard.State = document.ContainsKey(nameof(taskBoard.State)) ? document[nameof(taskBoard.State)].AsString().ToTaskState() : TaskState.Unknown;
                taskBoard.StartDate = document.ContainsKey(nameof(taskBoard.StartDate)) ? document[nameof(taskBoard.StartDate)].AsLong() : 0L;
                taskBoard.EndDate = document.ContainsKey(nameof(taskBoard.EndDate)) ? document[nameof(taskBoard.EndDate)].AsLong() : 0L;
                taskBoard.DueDate = document.ContainsKey(nameof(taskBoard.DueDate)) ? document[nameof(taskBoard.DueDate)].AsLong() : 0L;
                taskBoard.ResolvedTime = document.ContainsKey(nameof(taskBoard.ResolvedTime)) ? document[nameof(taskBoard.ResolvedTime)].AsLong() : 0L;
                taskBoard.ClosedTime = document.ContainsKey(nameof(taskBoard.ClosedTime)) ? document[nameof(taskBoard.ClosedTime)].AsLong() : 0L;
                taskBoard.Created = document.ContainsKey(nameof(taskBoard.Created)) ? document[nameof(taskBoard.Created)].AsLong() : 0L;
                taskBoard.Updated = document.ContainsKey(nameof(taskBoard.Updated)) ? document[nameof(taskBoard.Updated)].AsLong() : 0L;
                taskBoard.Deleted = document.ContainsKey(nameof(taskBoard.Deleted)) ? document[nameof(taskBoard.Deleted)].AsLong() : 0L;
                return taskBoard;
            } else {
                return null;
            }
        }

    }
}
