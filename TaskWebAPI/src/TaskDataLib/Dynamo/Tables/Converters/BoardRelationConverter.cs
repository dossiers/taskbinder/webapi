﻿using Amazon.DynamoDBv2.DocumentModel;
using HoloLC.TaskCoreLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskDataLib.Dynamo.Tables.Converters
{
    public static class BoardRelationConverter
    {
        public static Document ConvertToDocument(BoardRelation boardRelation)
        {
            if (boardRelation != null) {
                var document = new Document();
                document.Add(nameof(boardRelation.DependentBoardId), boardRelation.DependentBoardId);
                document.Add(nameof(boardRelation.LeadingBoardId), boardRelation.LeadingBoardId);
                if (boardRelation.LeadingBoardTitle != null) document.Add(nameof(boardRelation.LeadingBoardTitle), boardRelation.LeadingBoardTitle);
                document.Add(nameof(boardRelation.IsBlocked), boardRelation.IsBlocked);
                document.Add(nameof(boardRelation.Created), boardRelation.Created);
                document.Add(nameof(boardRelation.Updated), boardRelation.Updated);
                document.Add(nameof(boardRelation.Deleted), boardRelation.Deleted);
                return document;
            } else {
                return null;
            }
        }
        public static BoardRelation ConvertFromDocument(Document document)
        {
            if (document != null) {
                var boardRelation = new BoardRelation();
                boardRelation.DependentBoardId = document[nameof(boardRelation.DependentBoardId)].AsULong();
                boardRelation.LeadingBoardId = document[nameof(boardRelation.LeadingBoardId)].AsULong();
                boardRelation.LeadingBoardTitle = document.ContainsKey(nameof(boardRelation.LeadingBoardTitle)) ? document[nameof(boardRelation.LeadingBoardTitle)].AsString() : "";
                boardRelation.IsBlocked = document.ContainsKey(nameof(boardRelation.IsBlocked)) ? document[nameof(boardRelation.IsBlocked)].AsBoolean() : false;
                boardRelation.Created = document.ContainsKey(nameof(boardRelation.Created)) ? document[nameof(boardRelation.Created)].AsLong() : 0L;
                boardRelation.Updated = document.ContainsKey(nameof(boardRelation.Updated)) ? document[nameof(boardRelation.Updated)].AsLong() : 0L;
                boardRelation.Deleted = document.ContainsKey(nameof(boardRelation.Deleted)) ? document[nameof(boardRelation.Deleted)].AsLong() : 0L;
                return boardRelation;
            } else {
                return null;
            }
        }

    }
}
