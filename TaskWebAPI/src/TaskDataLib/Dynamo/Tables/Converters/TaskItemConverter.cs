﻿using Amazon.DynamoDBv2.DocumentModel;
using HoloLC.TaskCoreLib.Core;
using HoloLC.TaskCoreLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskDataLib.Dynamo.Tables.Converters
{
    public class TaskItemConverter
    {
        public static Document ConvertToDocument(TaskItem taskItem)
        {
            if (taskItem != null) {
                var document = new Document();
                document.Add(nameof(taskItem.Id), taskItem.Id);
                if (taskItem.Title != null) document.Add(nameof(taskItem.Title), taskItem.Title);
                if (taskItem.Description != null) document.Add(nameof(taskItem.Description), taskItem.Description);
                document.Add(nameof(taskItem.Board), taskItem.Board);
                document.Add(nameof(taskItem.CloneParent), taskItem.CloneParent);
                if (taskItem.ItemType.ToName() != null) document.Add(nameof(taskItem.ItemType), taskItem.ItemType.ToName());
                if (taskItem.ItemState.ToName() != null) document.Add(nameof(taskItem.ItemState), taskItem.ItemState.ToName());
                document.Add(nameof(taskItem.Priority), taskItem.Priority);
                document.Add(nameof(taskItem.Urgency), taskItem.Urgency);
                // document.Add(nameof(taskItem.Position", taskItem.Position);
                document.Add(nameof(taskItem.ZOrder), taskItem.ZOrder);
                document.Add(nameof(taskItem.Created), taskItem.Created);
                document.Add(nameof(taskItem.Updated), taskItem.Updated);
                document.Add(nameof(taskItem.Deleted), taskItem.Deleted);
                return document;
            } else {
                return null;
            }
        }
        public static TaskItem ConvertFromDocument(Document document)
        {
            if (document != null) {
                var taskItem = new TaskItem();
                taskItem.Id = document[nameof(taskItem.Id)].AsULong();
                taskItem.Title = document.ContainsKey(nameof(taskItem.Title)) ? document[nameof(taskItem.Title)].AsString() : "";
                taskItem.Description = document.ContainsKey(nameof(taskItem.Description)) ? document[nameof(taskItem.Description)].AsString() : "";
                taskItem.Board = document.ContainsKey(nameof(taskItem.Board)) ? document[nameof(taskItem.Board)].AsULong() : 0UL;
                taskItem.CloneParent = document.ContainsKey(nameof(taskItem.CloneParent)) ? document[nameof(taskItem.CloneParent)].AsULong() : 0UL;
                taskItem.ItemType = document.ContainsKey(nameof(taskItem.ItemType)) ? document[nameof(taskItem.ItemType)].AsString().ToTaskItemType() : TaskItemType.Unknown;
                taskItem.ItemState = document.ContainsKey(nameof(taskItem.ItemState)) ? document[nameof(taskItem.ItemState)].AsString().ToTaskItemState() : TaskItemState.Unknown;
                taskItem.Priority = document.ContainsKey(nameof(taskItem.Priority)) ? document[nameof(taskItem.Priority)].AsInt() : 0;
                taskItem.Urgency = document.ContainsKey(nameof(taskItem.Urgency)) ? document[nameof(taskItem.Urgency)].AsInt() : 0;
                // taskItem.Position = (ElementPosition)document[nameof(taskItem.Position)].AsInt();
                taskItem.ZOrder = document.ContainsKey(nameof(taskItem.ZOrder)) ? document[nameof(taskItem.ZOrder)].AsInt() : 0;
                taskItem.Created = document.ContainsKey(nameof(taskItem.Created)) ? document[nameof(taskItem.Created)].AsLong() : 0L;
                taskItem.Updated = document.ContainsKey(nameof(taskItem.Updated)) ? document[nameof(taskItem.Updated)].AsLong() : 0L;
                taskItem.Deleted = document.ContainsKey(nameof(taskItem.Deleted)) ? document[nameof(taskItem.Deleted)].AsLong() : 0L;
                return taskItem;
            } else {
                return null;
            }
        }

    }
}