﻿using HoloLC.TaskDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.TaskCoreLib.Models;
using NLog;
using AWSCore.DynamoBase.Tables.Core;
using AWSCore.DynamoBase.Repositories.Base;
using HoloLC.TaskDataLib.Dynamo.Tables.Converters;
using Amazon.DynamoDBv2.DocumentModel;

namespace HoloLC.TaskDataLib.Dynamo
{
    public class DynamoTaskItemRepository : BaseDynamoRepository, ITaskItemRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public DynamoTaskItemRepository(IDynamoTable dynamoTable)
            : base(dynamoTable)
        {
        }


        public async Task<TaskItem> GetItemAsync(ulong id, ulong board = 0)
        {
            // tbd:
            TaskItem taskItem = null;
            try {
                var document = await DynamoTable.Table?.GetItemAsync(id);
                taskItem = TaskItemConverter.ConvertFromDocument(document);
                Logger.Debug($"id = {id}. taskItem found: {taskItem}.");
            } catch (Exception ex) {
                Logger.Warn($"GetItemAsync() DynamoTable.Table?.GetItemAsync({id}) failed. {ex.Message}");
            }
            //if (board == 0) {
            //    try {
            //        var document = await DynamoTable.Table?.GetItemAsync(id);
            //        taskItem = TaskItemConverter.ConvertFromDocument(document);
            //        Logger.Debug($"id = {id}. taskItem found: {taskItem}.");
            //    } catch (Exception ex) {
            //        Logger.Warn($"GetItemAsync() DynamoTable.Table?.GetItemAsync({id}) failed. {ex.Message}");
            //    }
            //} else {
            //    try {
            //        var document = await DynamoTable.Table?.GetItemAsync(id, board);
            //        taskItem = TaskItemConverter.ConvertFromDocument(document);
            //        Logger.Debug($"id = {id}; board = {board}. taskItem found: {taskItem}.");
            //    } catch (Exception ex) {
            //        Logger.Warn($"GetItemAsync() DynamoTable.Table?.GetItemAsync({id},{board}) failed. {ex.Message}");
            //    }
            //}
            return taskItem;
        }

        // cf. https://blogs.aws.amazon.com/net/post/Tx1KWKHIN0894B/DynamoDB-Document-Model-Manual-Pagination
        public async Task<IEnumerable<TaskItem>> FindAllAsync(ulong board)
        {
            try {
                // TBD: No pagination at this point....
                //      --> This should be fixed ASAP.
                var search = DynamoTable.Table?.Query(new QueryOperationConfig {
                    Filter = new QueryFilter("Board", QueryOperator.Equal, board),
                    Limit = 1000,    // arbitrary, a large number to avoid pagination. TBD: Does it work??? What's the maximum value of Limit?
                });
                IList<Document> items = null;
                if (search != null) {
                    items = await search.GetNextSetAsync();
                }
                // var token = search.PaginationToken;
                if (items != null) {
                    var taskItems = new List<TaskItem>();
                    if (items.Any()) {
                        foreach (var d in items) {
                            var box = TaskItemConverter.ConvertFromDocument(d);
                            if (box != null) {
                                taskItems.Add(box);
                            } else {
                                // ???
                                Logger.Warn($"Failed to convert document to TaskItem. document = {d}");
                            }
                        }
                    }
                    Logger.Debug($"taskItems feteched. Count = {taskItems.Count}");
                    return taskItems;
                }
            } catch (Exception ex) {
                Logger.Warn($"FindAllAsync() (board = {board}) failed. {ex.Message}");
            }
            // ????
            Logger.Warn($"search.GetNextSetAsync() failed.");
            return null;
        }


        public async Task<bool> AddItemAsync(TaskItem taskItem)
        {
            Logger.Trace($"AddItemAsync() taskItem = {taskItem}.");

            try {
                var document = TaskItemConverter.ConvertToDocument(taskItem);
                if (document != null) {
                    await DynamoTable.Table?.UpdateItemAsync(document);
                    // TBD: How to check if UpdateItemAsync() was successful???
                    if (taskItem != null) {
                        Logger.Debug($"TaskItem saved: id = {taskItem.Id}. board = {taskItem.Board}.");
                        return true;
                    } else {
                        Logger.Info($"Table.UpdateItemAsync() failed.");
                        return false;
                    }
                }
            } catch (Exception ex) {
                Logger.Warn($"AddItemAsync() (taskItem = {taskItem}) failed. {ex.Message}");
            }
            Logger.Info($"AddItemAsync() failed.");
            return false;
        }

        public async Task<bool> ReplaceItemAsync(TaskItem taskItem, bool createIfNotExist = false)
        {
            Logger.Trace($"ReplaceItemAsync() taskItem = {taskItem}; createIfNotExist = {createIfNotExist}.");

            var saved = false;
            try {
                var id = taskItem.Id;
                var board = taskItem.Board;
                var existingDocument = await DynamoTable.Table?.GetItemAsync(id, board);
                if (existingDocument != null) {
                    var newDocument = TaskItemConverter.ConvertToDocument(taskItem);
                    if (newDocument != null) {
                        // newDocument = await DynamoTable.Table?.UpdateItemAsync(newDocument);
                        await DynamoTable.Table?.UpdateItemAsync(newDocument);
                    }
                    // tbd: How do you know if UpdateItemAsync() was successful???
                    if (newDocument != null) {
                        saved = true;
                        Logger.Debug($"TaskItem replaced: board = {board}. id = {id}.");
                    } else {
                        Logger.Warn($"Failed to replace TaskItem: board = {board}. id = {id}.");
                    }
                } else {
                    if (createIfNotExist) {
                        var newDocument = TaskItemConverter.ConvertToDocument(taskItem);
                        if (newDocument != null) {
                            // newDocument = await DynamoTable.Table?.UpdateItemAsync(newDocument);
                            await DynamoTable.Table?.UpdateItemAsync(newDocument);
                        }
                        // tbd: How do you know if UpdateItemAsync() was successful???
                        if (newDocument != null) {
                            saved = true;
                            Logger.Debug($"New pickupbox added: board = {board}. id = {id}.");
                        } else {
                            Logger.Warn($"Failed to add new TaskItem: board = {board}. id = {id}.");
                        }
                    } else {
                        Logger.Info($"TaskItem not found: board = {board}. id = {id}.");
                    }
                }
            } catch (Exception ex) {
                Logger.Warn($"ReplaceItemAsync() (taskItem = {taskItem}) failed. {ex.Message}");
            }
            return saved;
        }


        public async Task<TaskItem> DeleteItemAsync(ulong id, ulong board = 0)
        {
            Logger.Trace($"DeleteItemAsync() id = {id}.");

            try {
                var existingDocument = await DynamoTable.Table?.GetItemAsync(id);
                if (existingDocument != null) {
                    var taskItem = TaskItemConverter.ConvertFromDocument(existingDocument);
                    await DynamoTable.Table?.DeleteItemAsync(existingDocument);
                    // tbd: How do you know if DeleteItemAsync() was successful???
                    if (taskItem != null) {
                        Logger.Debug($"TaskItem deleted: board = {taskItem.Board}. id = {taskItem.Id}.");
                        return taskItem;
                    } else {
                        Logger.Warn($"Failed to delete pickupbox: id = {id}.");
                        return null;
                    }
                } else {
                    Logger.Info($"TaskItem not found for id = {id}.");
                    return null;
                }
            } catch (Exception ex) {
                Logger.Warn($"DeleteItemAsync() (id = {id}) failed. {ex.Message}");
                return null;
            }
        }

        public async Task<int> RemoveAllAsync(ulong board)
        {
            var count = 0;
            var list = await FindAllAsync(board);
            if (list != null) {
                foreach (var box in list) {
                    var d = TaskItemConverter.ConvertToDocument(box);
                    if (d != null) {
                        await DynamoTable.Table?.DeleteItemAsync(d);
                        count++;
                    }
                }
            }
            return count;
        }

    }
}
