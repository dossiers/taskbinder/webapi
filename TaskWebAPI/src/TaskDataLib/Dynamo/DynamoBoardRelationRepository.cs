﻿using HoloLC.TaskDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.TaskCoreLib.Models;
using NLog;
using AWSCore.DynamoBase.Tables.Core;
using AWSCore.DynamoBase.Repositories.Base;
using Amazon.DynamoDBv2.DocumentModel;
using HoloLC.TaskDataLib.Dynamo.Tables.Converters;

namespace HoloLC.TaskDataLib.Dynamo
{
    public class DynamoBoardRelationRepository : BaseDynamoRepository, IBoardRelationRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public DynamoBoardRelationRepository(IDynamoTable dynamoTable)
            : base(dynamoTable)
        {
        }


        // cf. https://blogs.aws.amazon.com/net/post/Tx1KWKHIN0894B/DynamoDB-Document-Model-Manual-Pagination
        public async Task<IEnumerable<BoardRelation>> FindLeadingBoardsAsync(ulong dependentBoardId)
        {
            try {
                // TBD: No pagination at this point....
                //      --> This should be fixed ASAP.
                var search = DynamoTable.Table?.Query(new QueryOperationConfig {
                    Filter = new QueryFilter("DependentBoardId", QueryOperator.Equal, dependentBoardId),
                    Limit = 1000,    // arbitrary, a large number to avoid pagination. TBD: Does it work??? What's the maximum value of Limit?
                });
                IList<Document> items = null;
                if (search != null) {
                    items = await search.GetNextSetAsync();
                }
                // var token = search.PaginationToken;
                if (items != null) {
                    var boardRelations = new List<BoardRelation>();
                    if (items.Any()) {
                        foreach (var d in items) {
                            var box = BoardRelationConverter.ConvertFromDocument(d);
                            if (box != null) {
                                boardRelations.Add(box);
                            } else {
                                // ???
                                Logger.Warn($"Failed to convert document to BoardRelation. document = {d}");
                            }
                        }
                    }
                    Logger.Debug($"boardRelations feteched. Count = {boardRelations.Count}");
                    return boardRelations;
                }
            } catch (Exception ex) {
                Logger.Warn($"FindTaskBoardsAsync() (binder = {dependentBoardId}) failed. {ex.Message}");
            }
            // ????
            Logger.Warn($"search.GetNextSetAsync() failed.");
            return null;
        }


        public async Task<bool> AddBoardRelationAsync(BoardRelation boardRelation)
        {
            Logger.Trace($"AddBoardRelationAsync() boardRelation = {boardRelation}.");

            try {
                var document = BoardRelationConverter.ConvertToDocument(boardRelation);
                if (document != null) {
                    await DynamoTable.Table?.UpdateItemAsync(document);
                    // TBD: How to check if UpdateItemAsync() was successful???
                    if (boardRelation != null) {
                        Logger.Debug($"BoardRelation saved: dependentBoardId = {boardRelation.LeadingBoardId}. leadingBoardId = {boardRelation.DependentBoardId}.");
                        return true;
                    } else {
                        Logger.Info($"Table.UpdateItemAsync() failed.");
                        return false;
                    }
                }
            } catch (Exception ex) {
                Logger.Warn($"AddBoardRelationAsync() (boardRelation = {boardRelation}) failed. {ex.Message}");
            }
            Logger.Info($"AddBoardRelationAsync() failed.");
            return false;
        }

        public async Task<bool> ReplaceBoardRelationAsync(BoardRelation boardRelation, bool createIfNotExist = false)
        {
            Logger.Trace($"ReplaceBoardRelationAsync() boardRelation = {boardRelation}; createIfNotExist = {createIfNotExist}.");

            var saved = false;
            try {
                var dependentBoardId = boardRelation.LeadingBoardId;
                var leadingBoardId = boardRelation.DependentBoardId;
                var existingDocument = await DynamoTable.Table?.GetItemAsync(dependentBoardId, leadingBoardId);
                if (existingDocument != null) {
                    var newDocument = BoardRelationConverter.ConvertToDocument(boardRelation);
                    if (newDocument != null) {
                        // newDocument = await DynamoTable.Table?.UpdateItemAsync(newDocument);
                        await DynamoTable.Table?.UpdateItemAsync(newDocument);
                    }
                    // tbd: How do you know if UpdateItemAsync() was successful???
                    if (newDocument != null) {
                        saved = true;
                        Logger.Debug($"BoardRelation replaced: leadingBoardId = {leadingBoardId}. dependentBoardId = {dependentBoardId}.");
                    } else {
                        Logger.Warn($"Failed to replace BoardRelation: leadingBoardId = {leadingBoardId}. dependentBoardId = {dependentBoardId}.");
                    }
                } else {
                    if (createIfNotExist) {
                        var newDocument = BoardRelationConverter.ConvertToDocument(boardRelation);
                        if (newDocument != null) {
                            // newDocument = await DynamoTable.Table?.UpdateItemAsync(newDocument);
                            await DynamoTable.Table?.UpdateItemAsync(newDocument);
                        }
                        // tbd: How do you know if UpdateItemAsync() was successful???
                        if (newDocument != null) {
                            saved = true;
                            Logger.Debug($"New pickupbox added: owner = {leadingBoardId}. id = {dependentBoardId}.");
                        } else {
                            Logger.Warn($"Failed to add new BoardRelation: owner = {leadingBoardId}. id = {dependentBoardId}.");
                        }
                    } else {
                        Logger.Info($"BoardRelation not found: owner = {leadingBoardId}. id = {dependentBoardId}.");
                    }
                }
            } catch (Exception ex) {
                Logger.Warn($"ReplaceBoardRelationAsync() (boardRelation = {boardRelation}) failed. {ex.Message}");
            }
            return saved;
        }


        public async Task<BoardRelation> DeleteBoardRelationAsync(ulong dependentBoardId, ulong leadingBoardId)
        {
            Logger.Trace($"DeleteBoardRelationAsync() dependentBoardId = {dependentBoardId}; leadingBoardId = {leadingBoardId}.");

            try {
                var existingDocument = await DynamoTable.Table?.GetItemAsync(dependentBoardId, leadingBoardId);
                if (existingDocument != null) {
                    var boardRelation = BoardRelationConverter.ConvertFromDocument(existingDocument);
                    await DynamoTable.Table?.DeleteItemAsync(existingDocument);
                    // tbd: How do you know if DeleteItemAsync() was successful???
                    if (boardRelation != null) {
                        Logger.Debug($"BoardRelation deleted: leadingBoardId = {boardRelation.DependentBoardId}. dependentBoardId = {boardRelation.LeadingBoardId}.");
                        return boardRelation;
                    } else {
                        Logger.Warn($"Failed to delete pickupbox: dependentBoardId = {dependentBoardId}; leadingBoardId = {leadingBoardId}.");
                        return null;
                    }
                } else {
                    Logger.Info($"BoardRelation not found for dependentBoardId = {dependentBoardId}; leadingBoardId = {leadingBoardId}.");
                    return null;
                }
            } catch (Exception ex) {
                Logger.Warn($"DeleteBoardRelationAsync() (dependentBoardId = {dependentBoardId}; leadingBoardId = {leadingBoardId}) failed. {ex.Message}");
                return null;
            }
        }

        public async Task<int> RemoveByDependentBoardAsync(ulong dependentBoardId)
        {
            var count = 0;
            var list = await FindLeadingBoardsAsync(dependentBoardId);
            if (list != null) {
                foreach (var box in list) {
                    var d = BoardRelationConverter.ConvertToDocument(box);
                    if (d != null) {
                        await DynamoTable.Table?.DeleteItemAsync(d);
                        count++;
                    }
                }
            }
            return count;
        }

    }
}
