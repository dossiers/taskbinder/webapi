﻿using HoloLC.TaskDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.TaskCoreLib.Models;
using NLog;
using AWSCore.DynamoBase.Tables.Core;
using AWSCore.DynamoBase.Repositories.Base;
using HoloLC.TaskDataLib.Dynamo.Tables.Converters;
using Amazon.DynamoDBv2.DocumentModel;

namespace HoloLC.TaskDataLib.Dynamo
{
    public class DynamoTaskBoardRepository : BaseDynamoRepository, ITaskBoardRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public DynamoTaskBoardRepository(IDynamoTable dynamoTable)
            : base(dynamoTable)
        {
        }


        public async Task<TaskBoard> GetItemAsync(ulong id, ulong owner = 0)
        {
            // tbd:
            TaskBoard taskBoard = null;
            try {
                var document = await DynamoTable.Table?.GetItemAsync(id);
                taskBoard = TaskBoardConverter.ConvertFromDocument(document);
                Logger.Debug($"id = {id}. taskBoard found: {taskBoard}.");
            } catch (Exception ex) {
                Logger.Warn($"GetItemAsync() DynamoTable.Table?.GetItemAsync({id}) failed. {ex.Message}");
            }
            return taskBoard;
        }

        // cf. https://blogs.aws.amazon.com/net/post/Tx1KWKHIN0894B/DynamoDB-Document-Model-Manual-Pagination
        public async Task<IEnumerable<TaskBoard>> FindAllByOwnerAsync(ulong owner)
        {
            try {
                // TBD: No pagination at this point....
                //      --> This should be fixed ASAP.
                var search = DynamoTable.Table?.Query(new QueryOperationConfig {
                    Filter = new QueryFilter("Owner", QueryOperator.Equal, owner),
                    Limit = 1000,    // arbitrary, a large number to avoid pagination. TBD: Does it work??? What's the maximum value of Limit?
                });
                IList<Document> items = null;
                if (search != null) {
                    items = await search.GetNextSetAsync();
                }
                // var token = search.PaginationToken;
                if (items != null) {
                    var taskBoards = new List<TaskBoard>();
                    if (items.Any()) {
                        foreach (var d in items) {
                            var box = TaskBoardConverter.ConvertFromDocument(d);
                            if (box != null) {
                                taskBoards.Add(box);
                            } else {
                                // ???
                                Logger.Warn($"Failed to convert document to TaskBoard. document = {d}");
                            }
                        }
                    }
                    Logger.Debug($"taskBoards feteched. Count = {taskBoards.Count}");
                    return taskBoards;
                }
            } catch (Exception ex) {
                Logger.Warn($"FindAllAsync() (owner = {owner}) failed. {ex.Message}");
            }
            // ????
            Logger.Warn($"search.GetNextSetAsync() failed.");
            return null;
        }


        public async Task<bool> AddItemAsync(TaskBoard taskBoard)
        {
            Logger.Trace($"AddItemAsync() taskBoard = {taskBoard}.");

            try {
                var document = TaskBoardConverter.ConvertToDocument(taskBoard);
                if (document != null) {
                    await DynamoTable.Table?.UpdateItemAsync(document);
                    // TBD: How to check if UpdateItemAsync() was successful???
                    if (taskBoard != null) {
                        Logger.Debug($"TaskBoard saved: id = {taskBoard.Id}. owner = {taskBoard.Owner}.");
                        return true;
                    } else {
                        Logger.Info($"Table.UpdateItemAsync() failed.");
                        return false;
                    }
                }
            } catch (Exception ex) {
                Logger.Warn($"AddItemAsync() (taskBoard = {taskBoard}) failed. {ex.Message}");
            }
            Logger.Info($"AddItemAsync() failed.");
            return false;
        }

        public async Task<bool> ReplaceItemAsync(TaskBoard taskBoard, bool createIfNotExist = false)
        {
            Logger.Trace($"ReplaceItemAsync() taskBoard = {taskBoard}; createIfNotExist = {createIfNotExist}.");

            var saved = false;
            try {
                var id = taskBoard.Id;
                var owner = taskBoard.Owner;
                var existingDocument = await DynamoTable.Table?.GetItemAsync(id, owner);
                if (existingDocument != null) {
                    var newDocument = TaskBoardConverter.ConvertToDocument(taskBoard);
                    if (newDocument != null) {
                        // newDocument = await DynamoTable.Table?.UpdateItemAsync(newDocument);
                        await DynamoTable.Table?.UpdateItemAsync(newDocument);
                    }
                    // tbd: How do you know if UpdateItemAsync() was successful???
                    if (newDocument != null) {
                        saved = true;
                        Logger.Debug($"TaskBoard replaced: owner = {owner}. id = {id}.");
                    } else {
                        Logger.Warn($"Failed to replace TaskBoard: owner = {owner}. id = {id}.");
                    }
                } else {
                    if (createIfNotExist) {
                        var newDocument = TaskBoardConverter.ConvertToDocument(taskBoard);
                        if (newDocument != null) {
                            // newDocument = await DynamoTable.Table?.UpdateItemAsync(newDocument);
                            await DynamoTable.Table?.UpdateItemAsync(newDocument);
                        }
                        // tbd: How do you know if UpdateItemAsync() was successful???
                        if (newDocument != null) {
                            saved = true;
                            Logger.Debug($"New pickupbox added: owner = {owner}. id = {id}.");
                        } else {
                            Logger.Warn($"Failed to add new TaskBoard: owner = {owner}. id = {id}.");
                        }
                    } else {
                        Logger.Info($"TaskBoard not found: owner = {owner}. id = {id}.");
                    }
                }
            } catch (Exception ex) {
                Logger.Warn($"ReplaceItemAsync() (taskBoard = {taskBoard}) failed. {ex.Message}");
            }
            return saved;
        }


        public async Task<TaskBoard> DeleteItemAsync(ulong id, ulong owner = 0)
        {
            Logger.Trace($"DeleteItemAsync() id = {id}.");

            try {
                var existingDocument = await DynamoTable.Table?.GetItemAsync(id);
                if (existingDocument != null) {
                    var taskBoard = TaskBoardConverter.ConvertFromDocument(existingDocument);
                    await DynamoTable.Table?.DeleteItemAsync(existingDocument);
                    // tbd: How do you know if DeleteItemAsync() was successful???
                    if (taskBoard != null) {
                        Logger.Debug($"TaskBoard deleted: owner = {taskBoard.Owner}. id = {taskBoard.Id}.");
                        return taskBoard;
                    } else {
                        Logger.Warn($"Failed to delete pickupbox: id = {id}.");
                        return null;
                    }
                } else {
                    Logger.Info($"TaskBoard not found for id = {id}.");
                    return null;
                }
            } catch (Exception ex) {
                Logger.Warn($"DeleteItemAsync() (id = {id}) failed. {ex.Message}");
                return null;
            }
        }

        public async Task<int> RemoveAllByOwnerAsync(ulong owner)
        {
            var count = 0;
            var list = await FindAllByOwnerAsync(owner);
            if (list != null) {
                foreach (var box in list) {
                    var d = TaskBoardConverter.ConvertToDocument(box);
                    if (d != null) {
                        await DynamoTable.Table?.DeleteItemAsync(d);
                        count++;
                    }
                }
            }
            return count;
        }

    }
}
